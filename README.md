# Social Media MERN Stack

## Développement d'une application Social Media website :

- Implémentation du module inscription, connexion et authentification.
- Mise en place des Routers de navigation.
- Gestion de pagination des produits.
- Trie des produits par champ
- Développement d’un module de recherche par catégorie.

## Mots Clés: NodeJS, ExpressJS, Mongo, API, JWT, Axios, JavaScript (DOM), React JS, Redux, Redux-thunk, Router, Bootstrap, Cloudinary, Git.

## Screenshot

### Register Page :

![Register](https://gitlab.com/yassine-ben-azouz/social-media-mern/-/raw/master/Screenshot/Register%20page.jpg)

### Login Page :

![Login](https://gitlab.com/yassine-ben-azouz/social-media-mern/-/raw/master/Screenshot/Login%20page.jpg)

<!-- ### Home page user not looged in :

![Home](https://gitlab.com/yassine-ben-azouz/shopping-cart-mern/-/raw/master/Screenshot/Home%20Page%20user%20not%20logged%20in.png) -->

### Home page user looged in :

![Home](https://gitlab.com/yassine-ben-azouz/social-media-mern/-/raw/master/Screenshot/posts%20page.jpg)

### Profil page :

![Profil](https://gitlab.com/yassine-ben-azouz/social-media-mern/-/raw/master/Screenshot/Profil%20page.jpg)

### Post Detail :

![Post](https://gitlab.com/yassine-ben-azouz/social-media-mern/-/raw/master/Screenshot/PostDetails.jpg)

### Admin dashboread :

![Admin dashboread](https://gitlab.com/yassine-ben-azouz/social-media-mern/-/raw/master/Screenshot/Admin%20Dashboread.jpg)

<!--
### Admin dashboread Users Page :

![Admin dashboread Users](https://gitlab.com/yassine-ben-azouz/shopping-cart-mern/-/raw/master/Screenshot/admin%20dashboread%20users%20page.jpg) -->
