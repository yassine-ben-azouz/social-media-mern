const jwt = require("jsonwebtoken");

exports.Authmiddleware = async (req, res, next) => {
  try {
    const token = req.headers.authorization;
    if (!token)
      return res
        .status(401)
        .json({ message: "you are not authorized no token" });
    const verifyToken = jwt.verify(
      token,
      process.env.SECRET_KEY,
      (error, decoded) => {
        if (error)
          return res
            .status(401)
            .json({ message: "you are not authorized, invalid token" });
        else return decoded;
      }
    );
    req.userId = verifyToken.sub;
    req.userRole = verifyToken.role;
    next();
  } catch (error) {
    res.status(500).json({ message: "something wrong" });
    console.log(error);
  }
};
