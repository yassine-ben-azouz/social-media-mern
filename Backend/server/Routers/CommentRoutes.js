const {
  createComment,
  deleteComment,
  updateComment,
} = require("../Controllers/CommentControllers");
const { Authmiddleware } = require("../Middlewares/Authmiddleware");
const { ValidateObjectId } = require("../Middlewares/ValidateObjectId");

//
const router = require("express").Router();
// create comment
router.post("/", Authmiddleware, createComment);
// updated comment
router.put("/:id", Authmiddleware, ValidateObjectId, updateComment);
// delete comment
router.delete("/:id", Authmiddleware, ValidateObjectId, deleteComment);

module.exports = router;
