const {
  toggleLike,
  updatePost,
  createPost,
  getAllPosts,
  getPost,
  deletePost,
} = require("../Controllers/PostsControllers");
const { Authmiddleware } = require("../Middlewares/Authmiddleware");
const { ValidateObjectId } = require("../Middlewares/ValidateObjectId");
const { upload } = require("../Middlewares/Multer.js");

const router = require("express").Router();
// create new post
router.post("/", Authmiddleware, upload.single("image"), createPost);
// get all posts
router.get("/:currentPage", getAllPosts);
// get post by id
router.get("/post/:id", Authmiddleware, ValidateObjectId, getPost);

// update post
router.put(
  "/:id",
  Authmiddleware,
  ValidateObjectId,
  upload.single("image"),
  updatePost
);
// toggle like
router.put("/like/:id", Authmiddleware, ValidateObjectId, toggleLike);

router.delete("/:id", Authmiddleware, ValidateObjectId, deletePost);

module.exports = router;
