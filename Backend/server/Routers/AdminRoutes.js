const {
  getAllComments,
  deleteComment,
} = require("../Controllers/CommentControllers");
const { insertManyPosts } = require("../Controllers/PostsControllers");
const {
  getAllUser,
  deleteAllUsers,
  deleteUser,
} = require("../Controllers/UsersControllers");
const { Authmiddleware } = require("../Middlewares/Authmiddleware");
const { isAdmin } = require("../Middlewares/isAdmin");
const { ValidateObjectId } = require("../Middlewares/ValidateObjectId");

const router = require("express").Router();

/*==== Users ======*/
router.get("/users", Authmiddleware, isAdmin, getAllUser);

router.delete("/users", Authmiddleware, isAdmin, deleteAllUsers);
router.delete(
  "/users/:id",
  Authmiddleware,
  isAdmin,
  ValidateObjectId,
  deleteUser
);
/*====// Users //======*/
/*==== Posts ======*/
router.post("/posts", insertManyPosts);
/*====// Posts //======*/

/*==== Comments ======*/
router.get("/comment", Authmiddleware, isAdmin, getAllComments);
router.delete(
  "/comment/:id",
  Authmiddleware,
  isAdmin,
  ValidateObjectId,
  deleteComment
);

/*====// Comments //======*/

module.exports = router;
