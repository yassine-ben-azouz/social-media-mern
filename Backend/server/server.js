const express = require("express");
const app = express();

require("dotenv").config();
require("./Config/ConnectDB");
const cors = require("cors");
app.use(cors());
app.use(express.json());
app.use("/api/v1/users/", require("./Routers/UsersRoutes"));
app.use("/api/v1/posts/", require("./Routers/PostRoutes"));
app.use("/api/v1/comment/", require("./Routers/CommentRoutes"));
app.use("/api/v1/admin/", require("./Routers/AdminRoutes"));

app.listen(process.env.PORT, () => {
  console.log("server is run on port 5000");
});
