import "bootstrap/dist/css/bootstrap.min.css";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import "react-toastify/dist/ReactToastify.css";

import "./App.css";
import Header from "./components/Header/Header";
import Routers from "./components/Routes/Routers";
import { getUsers } from "./redux/action/Users/userActions";
import { ToastContainer, Slide } from "react-toastify";
function App() {
  const dispatch = useDispatch();

  const { usersList } = useSelector((state) => state.users);
  // console.log(userList);
  return (
    <div>
      <Header />
      <Routers />
      <div></div>

      <ToastContainer
        position={"bottom-right"}
        closeOnClick={true}
        transition={Slide}
        autoClose={"3000"}
        theme="colored"
        bodyStyle={{ color: "white" }}
      />
    </div>
  );
}

export default App;
