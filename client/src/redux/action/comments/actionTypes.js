/*======== CREATE_COMMENT ==============*/
export const CREATE_COMMENT_FULFILLED = "CREATE_COMMENT_FULFILLED";
export const CREATE_COMMENT_REJECTED = "CREATE_COMMENT_REJECTED";

/*========// CREATE_COMMENT //==============*/

export const TOGGLE_COMMENT = "TOGGLE_COMMENT";
