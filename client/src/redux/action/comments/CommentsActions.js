import axios from "axios";
import { getPost } from "../Posts/postsAction";
import { toast } from "react-toastify";
import { TOGGLE_COMMENT } from "./actionTypes";

export const createComment = (data) => async (dispatch) => {
  try {
    const { PostId } = data;
    const id = PostId;
    const response = await axios.post("/comment/", data, {
      headers: {
        authorization: localStorage.getItem("token"),
      },
    });
    toast.success(response.data.message);
    dispatch(getPost(id));
  } catch (error) {
    toast.error(
      error.response && error.response.data.message
        ? error.response.data.message
        : error.response
    );
  }
};

export const toggleComments = () => {
  return {
    type: TOGGLE_COMMENT,
  };
};

export const deleteComment = (data) => async (dispatch) => {
  try {
    const { id, commentId } = data;
    const response = await axios.delete(`/comment/${commentId}`, {
      headers: {
        authorization: localStorage.getItem("token"),
      },
    });
    dispatch(getPost(id));
    toast.success(response.data.message);
  } catch (error) {
    toast.error(
      error.response && error.response.data.message
        ? error.response.data.message
        : error.response
    );
  }
};

export const EditComment = (data) => async (dispatch) => {
  try {
    const { commentId, text, id } = data;
    const response = await axios.put(
      `/comment/${commentId}`,
      { text },
      {
        headers: { authorization: localStorage.getItem("token") },
      }
    );
    dispatch(getPost(id));
    toast.success(response.data.message);
  } catch (error) {
    toast.error(
      error.response && error.response.data.message
        ? error.response.data.message
        : error.response
    );
  }
};
