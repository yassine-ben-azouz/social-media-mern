/*==== GET ALL USER info ====*/
export const GET_All_USERS_PENDING = "GET_All_USERS_PENDING";
export const GET_All_USERS_FULFILLED = "GET_All_USERS_FULFILLED";
export const GET_All_USERS_REJECTED = "GET_All_USERS_REJECTED";
/*====// GET ALL USER info //====*/

/*==== GET ONE USER info ====*/
export const GET_USER_PENDING = "GET_USER_PENDING";
export const GET_USER_FULFILLED = "GET_USER_FULFILLED";
export const GET_USER_REJECTED = "GET_USER_REJECTED";
/*====// GET ONE USER info //====*/

/*==== REGISTER USER ====*/
export const REGISTER_USER_FULFILLED = "REGISTER_USER_FULFILLED";
export const REGISTER_USER_REJECTED = "REGISTER_USER_REJECTED";
/*====// REGISTER USER //====*/

/*==== LOGIN ====*/
export const LOGIN_USER_FULFILLED = "LOGIN_USER_FULFILLED";
export const LOGIN_USER_REJECTED = "LOGIN_USER_REJECTED";
/*====// LOGIN //====*/

/*======= LOGUT =======*/
export const LOGOUT_USER_FULFILLED = "LOGOUT_USER_FULFILLED";
/*======// LOGUT //======*/

/*======= UPLOEAD_USER_PHOTO =======*/
export const UPLOEAD_USER_PHOTO_REJECTED = "UPLOEAD_USER_PHOTO_REJECTED";
/*======// UPLOEAD_USER_PHOTO //======*/

/*======= DELETE_USER_REJECTED =======*/
export const DELETE_USER_REJECTED = "DELETE_USER_REJECTED";
/*======// DELETE_USER_REJECTED //======*/
