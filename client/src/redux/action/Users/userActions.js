import axios from "axios";
import jwt_decode from "jwt-decode";

import {
  GET_All_USERS_PENDING,
  GET_All_USERS_FULFILLED,
  GET_All_USERS_REJECTED,
  REGISTER_USER_REJECTED,
  REGISTER_USER_FULFILLED,
  LOGIN_USER_FULFILLED,
  LOGIN_USER_REJECTED,
  GET_USER_FULFILLED,
  GET_USER_PENDING,
  GET_USER_REJECTED,
  LOGOUT_USER_FULFILLED,
  UPLOEAD_USER_PHOTO_REJECTED,
} from "./actionsTaypes.js";
import { toast } from "react-toastify";
/*==== getAllUsers ====*/
export const getAllUsers = () => async (dispatch) => {
  dispatch({
    type: GET_All_USERS_PENDING,
  });
  try {
    const response = await axios.get(`/admin/users/`, {
      headers: {
        authorization: localStorage.getItem("token"),
      },
    });
    return dispatch({
      type: GET_All_USERS_FULFILLED,
      payload: response.data,
    });
  } catch (error) {
    return dispatch({
      type: GET_All_USERS_REJECTED,
      payload: error,
    });
  }
};
/*====// getAllUsers //====*/

/*==== RegisterUser ====*/
export const RegisterUser = (data) => async (dispatch) => {
  try {
    let response = await axios.post("/users/register", data);
    toast.success(response.data.message);
    const token = response.data.token;
    const decoded = jwt_decode(token);
    localStorage.setItem("token", token);
    localStorage.setItem("isAuth", true);
    localStorage.setItem("userId", decoded.sub);

    if (decoded.role === "admin") {
      response = { token, isAdmin: true, id: decoded.sub };
      localStorage.setItem("isAdmin", true);
    } else {
      response = { token, isAdmin: false, id: decoded.sub };
      localStorage.setItem("isAdmin", false);
    }
    return dispatch({
      type: REGISTER_USER_FULFILLED,
      payload: response,
    });
  } catch (error) {
    console.log(error);
    toast.error(
      error.response && error.response.data.message
        ? error.response.data.message
        : error.response
    );

    dispatch({
      type: REGISTER_USER_REJECTED,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response,
    });
  }
};
/*====// RegisterUser //====*/

/*==== loginUser ====*/
export const loginUser = (data) => async (dispatch) => {
  try {
    let response = await axios.post("/users/login/", data);
    toast.success(response.data.message);
    const token = response.data.token;
    localStorage.setItem("token", token);
    localStorage.setItem("isAuth", true);

    const decoded = jwt_decode(token);
    localStorage.setItem("userId", decoded.sub);

    if (decoded.role === "admin") {
      localStorage.setItem("isAdmin", true);
      response = { token, isAdmin: true, id: decoded.sub };
    } else {
      localStorage.setItem("isAdmin", false);
      response = { token, isAdmin: false, id: decoded.sub };
    }

    return dispatch({
      type: LOGIN_USER_FULFILLED,
      payload: response,
    });
  } catch (error) {
    toast.error(
      error.response && error.response.data.message
        ? error.response.data.message
        : error.response
    );

    return dispatch({
      type: LOGIN_USER_REJECTED,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response,
    });
  }
};
/*====// loginUser //====*/

/*==== getUserInfo ====*/
export const getUserInfo = (id) => async (dispatch) => {
  try {
    dispatch({ type: GET_USER_PENDING });
    const response = await axios.get(`/users/${id}`, {
      headers: {
        authorization: localStorage.getItem("token"),
      },
    });
    return dispatch({
      type: GET_USER_FULFILLED,
      payload: response.data,
    });
  } catch (error) {
    dispatch({
      type: GET_USER_REJECTED,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response,
    });
  }
};
/*====// getUserInfo //====*/

/*==== logutUser ====*/
export const logutUser = () => (dispatch) => {
  toast.success("user logout successfully");
  localStorage.removeItem("token");
  localStorage.setItem("isAuth", false);
  localStorage.setItem("isAdmin", false);
  localStorage.removeItem("userId");

  return dispatch({
    type: LOGOUT_USER_FULFILLED,
  });
};
/*====// logutUser //====*/

/*==== UploadPhotoProfile ====*/
export const UploadPhotoProfile = (data) => async (dispatch) => {
  console.log("data", data);
  const { userId: id, imageInput } = data;
  try {
    const form = new FormData();
    form.append("image", imageInput);
    const response = await axios.put(`/users/register/photo/`, form, {
      headers: { authorization: localStorage.getItem("token") },
    });
    toast.success(response.data.message);
    dispatch(getUserInfo(id));
  } catch (error) {
    toast.error(
      error.response && error.response.data.message
        ? error.response.data.message
        : error.response
    );
    dispatch({
      type: UPLOEAD_USER_PHOTO_REJECTED,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response,
    });
  }
};
/*====// UploadPhotoProfile //====*/

/*==== removeUser ====*/
export const deleteUser = (id) => async (dispatch) => {
  try {
    dispatch({ type: GET_USER_PENDING });
    const response = await axios.delete(`/admin/users/${id}`, {
      headers: {
        authorization: localStorage.getItem("token"),
      },
    });
    toast.success(response.data.message);
    return dispatch(getAllUsers());
  } catch (error) {
    dispatch({
      type: GET_USER_REJECTED,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response,
    });
  }
};
/*====// removeUser //====*/
