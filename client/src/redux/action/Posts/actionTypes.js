/*=========== ADD_POST_REJECTED ==============*/
export const ADD_POST_REJECTED = "ADD_POST_REJECTED";
/*===========// ADD_POST_REJECTED //==============*/

/*=========== GET_ALL_POSTS ==============*/
export const GET_ALL_POSTS_PENDING = "GET_ALL_POSTS_PENDING";
export const GET_ALL_POSTS_FULFILLED = "GET_ALL_POSTS_FULFILLED";
export const GET_ALL_POSTS_REJECTED = "GET_ALL_POSTS_PENDING";
/*===========// GET_ALL_POSTS //==============*/

/*=========== GET_POST ==============*/
export const GET_POST_PENDING = "GET_POST_PENDING";
export const GET_POST_FULFILLED = "GET_POST_FULFILLED";
export const GET_POST_REJECTED = "GET_POST_REJECTED";
/*===========// GET_POST //==============*/

/*=========== UPDATE_POSTS_POSTS ==============*/
export const UPDATE_POSTS_REJECTED = "UPDATE_POSTS_REJECTED";
/*===========// UPDATE_POSTS_POSTS //==============*/

/*=========== TOGGLE_LIKE ==============*/
export const TOGGLE_LIKE_FULFILLED = "TOGGLE_LIKE_FULFILLED";
export const TOGGLE_LIKE_REJECTED = "TOGGLE_LIKE_REJECTED";
/*===========// TOGGLE_LIKE //==============*/

/*=========== DELETE_POST ==============*/
export const DELETE_POST_REJECTED = "DELETE_POST_REJECTED";
/*===========// DELETE_POST //==============*/
