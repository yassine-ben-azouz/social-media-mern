import axios from "axios";
import { toast } from "react-toastify";
import {
  GET_ALL_POSTS_FULFILLED,
  GET_ALL_POSTS_PENDING,
  GET_ALL_POSTS_REJECTED,
  GET_POST_FULFILLED,
  GET_POST_PENDING,
  GET_POST_REJECTED,
  ADD_POST_REJECTED,
  DELETE_POST_REJECTED,
  UPDATE_POSTS_REJECTED,
} from "./actionTypes";
/*======== addNewPost ======*/
export const addNewPost = (data) => async (dispatch) => {
  try {
    console.log("data", data);
    const form = new FormData();
    const { title, description, category, imageUpload } = data;
    form.append("title", title);
    form.append("description", description);
    form.append("category", category);
    form.append("image", imageUpload);

    const response = await axios.post(`/posts/`, form, {
      headers: { authorization: localStorage.getItem("token") },
    });
    dispatch(getAllPosts());
    toast.success(response.data.message);
  } catch (error) {
    toast.error(
      error.response && error.response.data.message
        ? error.response.data.message
        : error.response
    );
    dispatch({
      type: ADD_POST_REJECTED,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response,
    });
  }
};
/*========// addNewPost //======*/

/*======== getAllPosts ======*/
export const getAllPosts = (currentPage) => async (dispatch) => {
  try {
    dispatch({
      type: GET_ALL_POSTS_PENDING,
    });
    console.log(currentPage);
    const response = await axios.get(`/posts/${currentPage}`);
    dispatch({
      type: GET_ALL_POSTS_FULFILLED,
      payload: response.data,
    });
  } catch (error) {
    dispatch({
      type: GET_ALL_POSTS_REJECTED,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response,
    });
  }
};
/*========// getAllPosts  //======*/

/*======== getAllPosts ======*/
export const getPost = (id) => async (dispatch) => {
  try {
    dispatch({
      type: GET_POST_PENDING,
    });
    const response = await axios.get(`/posts/post/${id}`, {
      headers: {
        authorization: localStorage.getItem("token"),
      },
    });
    dispatch({
      type: GET_POST_FULFILLED,
      payload: response.data,
    });
  } catch (error) {
    dispatch({
      type: GET_POST_REJECTED,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response,
    });
  }
};
/*========// getAllPosts  //======*/

/*======== ToggleLike ======*/
export const ToggleLike = (id) => async (dispatch) => {
  try {
    const data = "";
    const response = await axios.put(`/posts/like/${id}`, data, {
      headers: {
        authorization: localStorage.getItem("token"),
      },
    });
    toast.success(response.data.message);
    dispatch(getAllPosts());
  } catch (error) {
    console.log(
      error.response && error.response.data.message
        ? error.response.data.message
        : error.response
    );
  }
};
/*========// ToggleLike //======*/

/*======== update ======*/
export const updatePost = (data) => async (dispatch) => {
  try {
    const { id, title, description, category } = data;
    const form = new FormData();
    form.append("title", title);
    form.append("image", data.saveImage);
    form.append("description", description);
    form.append("category", category);

    const response = await axios.put(`/posts/${id}`, form, {
      headers: {
        authorization: localStorage.getItem("token"),
      },
    });
    toast.success(response.data.message);
    dispatch(getAllPosts());
  } catch (error) {
    toast.error(
      error.response && error.response.data.message
        ? error.response.data.message
        : error.response
    );
    dispatch({
      type: UPDATE_POSTS_REJECTED,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response,
    });
  }
};
/*======== update ======*/

/*======== getAllPosts ======*/
export const removePost = (id) => async (dispatch) => {
  try {
    const response = await axios.delete(`/posts/${id}`, {
      headers: {
        authorization: localStorage.getItem("token"),
      },
    });

    dispatch(getAllPosts());
    toast.success(response.data.message);
  } catch (error) {
    toast.error(
      error.response && error.response.data.message
        ? error.response.data.message
        : error.response
    );
    dispatch({
      type: DELETE_POST_REJECTED,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.response,
    });
  }
};
/*========// getAllPosts  //======*/
