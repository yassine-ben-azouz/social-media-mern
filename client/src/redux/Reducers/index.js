import { combineReducers } from "redux";
import postReducer from "./PostReducer";
import UserReducer from "./UserReducer";
export default combineReducers({ users: UserReducer, posts: postReducer });
