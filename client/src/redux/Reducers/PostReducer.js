import { TOGGLE_COMMENT } from "../action/comments/actionTypes";
import {
  ADD_POST_REJECTED,
  DELETE_POST_REJECTED,
  GET_ALL_POSTS_FULFILLED,
  GET_ALL_POSTS_PENDING,
  GET_ALL_POSTS_REJECTED,
  GET_POST_FULFILLED,
  GET_POST_PENDING,
  GET_POST_REJECTED,
  UPDATE_POSTS_REJECTED,
} from "../action/Posts/actionTypes";

const initialstate = {
  postList: [],
  loading: false,
  errors: null,
  postInfo: null,
  toggleComment: false,
  pagesCount: 2,
  PostsCount: 1,
};

const postReducer = (state = initialstate, { type, payload }) => {
  switch (type) {
    /*====== ADD_POST_REJECTED ==========*/
    case ADD_POST_REJECTED:
      return {
        ...state,
        errors: payload,
      };
    /*======// ADD_POST_REJECTED //==========*/

    /*====== GET_ALL_POSTS ==========*/
    case GET_ALL_POSTS_PENDING:
      return {
        ...state,
        loading: true,
        errors: null,
      };
    case GET_ALL_POSTS_FULFILLED:
      return {
        postList: payload.posts,
        pagesCount: payload.pagesCount,
        errors: null,
        loading: false,
        PostsCount: payload.PostsCount,
      };
    case GET_ALL_POSTS_REJECTED:
      return {
        ...state,
        errors: payload,
        loading: false,
      };
    /*======// GET_ALL_POSTS //==========*/

    /*====== GET_POST ==========*/
    case GET_POST_PENDING:
      return {
        ...state,
        loading: true,
      };
    case GET_POST_FULFILLED:
      return {
        ...state,
        errors: null,
        loading: false,

        postInfo: payload,
      };
    case GET_POST_REJECTED:
      return {
        ...state,
        errors: payload,
      };
    /*======// GET_POST //==========*/

    /*====== UPDATE_POSTS_REJECTED ==========*/
    case UPDATE_POSTS_REJECTED:
      return {
        ...state,
        errors: payload,
      };
    /*======// UPDATE_POSTS_REJECTED //==========*/

    case DELETE_POST_REJECTED:
      return {
        ...state,
        errors: payload,
      };

    case TOGGLE_COMMENT:
      return { ...state, toggleComment: !state.toggleComment };

    default:
      return { ...state };
  }
};

export default postReducer;
