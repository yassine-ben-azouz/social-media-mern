import {
  GET_All_USERS_PENDING,
  GET_All_USERS_FULFILLED,
  GET_All_USERS_REJECTED,
  REGISTER_USER_REJECTED,
  REGISTER_USER_FULFILLED,
  LOGIN_USER_FULFILLED,
  LOGIN_USER_REJECTED,
  LOGOUT_USER_FULFILLED,
  GET_USER_PENDING,
  GET_USER_FULFILLED,
  GET_USER_REJECTED,
} from "../action/Users/actionsTaypes";

const initialstate = {
  usersList: [],
  userInfo: null,
  userId: localStorage.getItem("userId") || null,
  isAuth: JSON.parse(localStorage.getItem("isAuth")) || false,
  isAdmin: JSON.parse(localStorage.getItem("isAdmin")) || false,
  token: localStorage.getItem("token") || null,
  errors: null,
  loading: false,
};
const UserReducer = (state = initialstate, { type, payload }) => {
  /*===== GET_All_USER =====*/
  switch (type) {
    case GET_All_USERS_PENDING:
      return { ...state, loading: true };
    case GET_All_USERS_FULFILLED:
      return { ...state, loading: false, usersList: payload };
    case GET_All_USERS_REJECTED:
      return { ...state, loading: false, errors: payload };
    /*===== GET_All_USER =====*/

    /*===== REGISTER =====*/
    case REGISTER_USER_FULFILLED:
      return {
        loading: false,
        token: payload.token,
        role: payload.role,
        isAuth: true,
        isAdmin: payload.isAdmin,
        userId: payload.id,
      };
    case REGISTER_USER_REJECTED:
      return {
        loading: false,
        errors: payload,
      };
    /*=====// REGISTER //=====*/

    /*===== LOGIN =====*/
    case LOGIN_USER_FULFILLED:
      return {
        loading: false,
        token: payload.token,
        isAuth: true,
        isAdmin: payload.isAdmin,
        userId: payload.id,
      };
    case LOGIN_USER_REJECTED:
      return {
        loading: false,
        errors: payload,
      };
    /*=====// LOGIN //=====*/
    /*===== LOGOUT =====*/
    case LOGOUT_USER_FULFILLED:
      return {
        token: null,
        isAuth: false,
        isAdmin: false,
        userId: null,
      };
    /*=====// LOGOUT //=====*/

    /*===== GET ONE USER INFO =====*/
    case GET_USER_PENDING:
      return {
        ...state,
        errors: null,
        loading: true,
      };
    case GET_USER_FULFILLED:
      return {
        ...state,
        userInfo: payload,
        errors: null,
        loading: false,
      };
    case GET_USER_REJECTED:
      return {
        ...state,
        errors: true,
        loading: false,
      };
    /*=====// GET ONE USER INFO //=====*/

    default:
      return state;
  }
};
export default UserReducer;
