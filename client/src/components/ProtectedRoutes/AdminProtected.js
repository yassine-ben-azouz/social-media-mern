import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Navigate, Outlet } from "react-router-dom";
import { getUserInfo } from "../../redux/action/Users/userActions";

const AdminProtected = () => {
  const { isAdmin, userId } = useSelector((state) => state.users);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getUserInfo(userId));
  }, [isAdmin]);
  return <div>{isAdmin ? <Outlet /> : <Navigate to="/" />}</div>;
};

export default AdminProtected;
