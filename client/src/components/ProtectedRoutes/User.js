import React from "react";
import { useSelector } from "react-redux";
import { Outlet, Navigate } from "react-router-dom";

const User = () => {
  const { isAuth } = useSelector((state) => state.users);
  console.log("isAuth", isAuth);
  return <div>{isAuth ? <Outlet /> : <Navigate to="/login" />}</div>;
};

export default User;
