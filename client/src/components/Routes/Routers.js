import React from "react";
import { Route, Routes } from "react-router-dom";
import Admin from "../../pages/Admin/Admin";
import Users from "../../pages/Admin/pages/Users/Users";
import Home from "../../pages/Home/Home";
import PostDetails from "../../pages/Home/postdetails/PostDetails";
import Login from "../../pages/Login/Login";
import Profil from "../../pages/Profil/Profil";
import Register from "../../pages/Register/Register";
import AdminProtected from "../ProtectedRoutes/AdminProtected";
import User from "../ProtectedRoutes/User";

const Routers = () => {
  return (
    <div>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route element={<User />}>
          <Route path="/posts/postdetails/:id" element={<PostDetails />} />
          <Route path="/profil/:id" element={<Profil />} />
        </Route>
        <Route element={<AdminProtected />}>
          <Route path="/admin" element={<Admin />} />
          <Route path="/admin/users" element={<Users />} />
          <Route path="/admin/profil/:id" element={<Profil />} />
        </Route>
        <Route path="/register" element={<Register />} />
        <Route path="/login" element={<Login />} />
      </Routes>
    </div>
  );
};

export default Routers;
