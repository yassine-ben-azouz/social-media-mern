import React, { useEffect } from "react";
import "./Header.css";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { logutUser } from "../../redux/action/Users/userActions";
import { LOGOUT_USER } from "../../redux/action/Users/actionsTaypes";
const Header = () => {
  const { isAuth, isAdmin, userId } = useSelector((state) => state.users);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const Logout = (e) => {
    e.preventDefault();
    dispatch(logutUser());
  };
  useEffect(() => {}, [isAuth]);
  return (
    <header>
      <div className="container p-3">
        <div className="row">
          <div className="col-4">
            <Link to="/" className="header-brand">
              social media
            </Link>
          </div>
          <div className="col-8 d-flex align-items-center justify-content-end ">
            <nav className={isAuth ? "nav-links-isAuth" : "nav-links"}>
              {isAuth ? (
                <>
                  {isAdmin && (
                    <Link to="/admin" className="link">
                      admin
                    </Link>
                  )}
                  <Link to={`/profil/${userId}`} className="link">
                    profil
                  </Link>
                  <button className="btn btn-secondary" onClick={Logout}>
                    logout
                  </button>
                </>
              ) : (
                <>
                  <Link to="/register" className="link">
                    rgister
                  </Link>
                  <Link to="/login" className="link">
                    login
                  </Link>
                </>
              )}
            </nav>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
