import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { createComment } from "../../../../redux/action/comments/CommentsActions";
import CommentCard from "./CommentCard";

const Comments = (props) => {
  const { postInfo } = props;
  const [text, setText] = useState("");
  const dispatch = useDispatch();
  const PostId = postInfo?._id;
  const sendData = (e) => {
    e.preventDefault();
    dispatch(createComment({ text, PostId }));
  };
  return (
    <div className="comment">
      {postInfo ? (
        postInfo?.comments.map((comment) => (
          <CommentCard comment={comment} key={Math.random()} id={PostId} />
        ))
      ) : (
        <h1>no comments</h1>
      )}
      <div className="comment-action">
        <input
          type="text"
          placeholder="create new comment"
          className="create-new-comment-input"
          onChange={(e) => setText(e.target.value)}
        />
        <button className="btn btn-primary" onClick={sendData}>
          add
        </button>
      </div>
    </div>
  );
};

export default Comments;
