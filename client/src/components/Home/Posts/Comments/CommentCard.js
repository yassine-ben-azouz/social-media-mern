import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { deleteComment } from "../../../../redux/action/comments/CommentsActions";
import UpdateComment from "./Modals/UpdateComment";

const CommentCard = (props) => {
  const { comment, id } = props;
  const { userId, isAdmin } = useSelector((state) => state.users);
  const checkOwner = comment.owner === userId;
  const dispatch = useDispatch();
  const removeComment = (commentId) => {
    dispatch(deleteComment({ commentId, id }));
  };

  return (
    <div>
      <div className="comment-container">
        <img
          className="comment-owner-image"
          src={comment.image}
          alt={comment.username}
        />{" "}
        <div className="comment-acton-card">
          <h5 className="comment-title">{comment.text}</h5>
          <div className="action">
            {checkOwner ? (
              <>
                <UpdateComment commentId={comment._id} id={id} />
                <button
                  className="btn btn-danger"
                  onClick={() => removeComment(comment._id)}
                >
                  delete
                </button>
              </>
            ) : (
              isAdmin && (
                <>
                  <button
                    className="btn btn-danger"
                    onClick={() => removeComment(comment._id)}
                  >
                    delete
                  </button>
                </>
              )
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default CommentCard;
