import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import { useDispatch } from "react-redux";
import { EditComment } from "../../../../../redux/action/comments/CommentsActions";
const UpdateComment = (props) => {
  const { commentId, id } = props;
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [text, setText] = useState();
  const dispatch = useDispatch();
  const sendData = (e) => {
    e.preventDefault();
    dispatch(EditComment({ text, commentId, id }));
    handleClose();
  };
  return (
    <div>
      <>
        <Button
          variant=""
          className="btn btn-secondary mx-3 btn-update-comment"
          onClick={handleShow}
        >
          update
        </Button>

        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>update comments</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlInput1"
              >
                <Form.Control
                  type="text"
                  placeholder="set new comment"
                  autoFocus
                  onChange={(e) => setText(e.target.value)}
                />
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="primary" onClick={sendData}>
              updated
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    </div>
  );
};

export default UpdateComment;
