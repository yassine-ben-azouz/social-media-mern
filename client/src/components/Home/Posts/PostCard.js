import React from "react";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import { useDispatch, useSelector } from "react-redux";
import {
  removePost,
  ToggleLike,
} from "../../../redux/action/Posts/postsAction";
import EditPost from "./Modals/EditPost";
import { Link, useNavigate } from "react-router-dom";
import DeletePost from "./Modals/DeletePost";
const PostCard = (props) => {
  const { post, profil } = props;
  const { isAuth, isAdmin, userId, userInfo } = useSelector(
    (state) => state.users
  );
  const checkLike = post.likes.indexOf(userId?.toString()) + 1;
  const checkOwner = post.owner._id === userId;
  const countLikes = post.likes.length;
  const commentsCount = post.comments?.length;
  const dispatch = useDispatch();
  const toggle = () => {
    dispatch(ToggleLike(post._id));
  };

  const navigate = useNavigate();
  return (
    <div className="col-10 my-4 ">
      <Card className="text-center post-card">
        <div className="container-post">
          <Card.Header className="header-post">
            <Link to={`/profil/${post.owner?._id}`} className="user-info">
              <h1 className="header-username-post">{post.owner.username}</h1>
              {/* {check if is profile page} */}
              {profil ? (
                <img
                  src={userInfo.image?.url}
                  alt={post.owner.username}
                  className="post-header-image"
                />
              ) : (
                <img
                  src={post.owner.image?.url}
                  alt={post.owner.username}
                  className="post-header-image"
                />
              )}
            </Link>
            {checkOwner ? (
              <div className="post-header-action">
                <EditPost id={post._id} />
                <DeletePost post={post} />
              </div>
            ) : (
              isAdmin && <button className="btn btn-danger">delete</button>
            )}
          </Card.Header>

          <Card.Body className="post-body">
            <Card.Title>{post.title}</Card.Title>

            <img src={post.image.url} alt={post.title} className="image-post" />
            {/* <Card.Text>{post.description}</Card.Text> */}
            {isAuth && (
              <Button
                variant="primary btn-details"
                onClick={() => navigate(`/posts/postdetails/${post._id}`)}
              >
                see details
              </Button>
            )}
          </Card.Body>
          <Card.Footer className="text-muted">2 days ago</Card.Footer>
          {isAuth && (
            <div className="post-action">
              <div className="post-action-likes">
                <i
                  className={
                    checkLike
                      ? "fa fa-thumbs-up like-icon"
                      : "fa fa-thumbs-up  unlike-icon"
                  }
                  aria-hidden="true"
                  onClick={toggle}
                ></i>
                <p className="count-likes">{!!countLikes && countLikes}</p>
              </div>

              <p className="post-action-comment">
                comments {!!commentsCount && commentsCount}
              </p>
            </div>
          )}
        </div>
      </Card>
    </div>
  );
};

export default PostCard;
