import React from "react";
import { useSelector } from "react-redux";
import AddPost from "./Modals/AddPost";
import PostCard from "./PostCard";
import "./posts.css";
const PositList = (props) => {
  const { postList, profil } = props;
  console.log(profil);
  const { isAuth } = useSelector((state) => state.users);
  console.log(isAuth);

  return (
    <div className="container">
      {isAuth && <AddPost />}

      <div className="row">
        {postList.length ? (
          postList.map((post) => (
            <PostCard post={post} key={post._id} profil={profil} />
          ))
        ) : (
          <h1>no post</h1>
        )}
      </div>
    </div>
  );
};

export default PositList;
