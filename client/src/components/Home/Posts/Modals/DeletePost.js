import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import { useDispatch } from "react-redux";
import { removePost } from "../../../../redux/action/Posts/postsAction";

const DeletePost = (props) => {
  const { _id, title } = props.post;
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const dispatch = useDispatch();
  const deltePost = () => {
    dispatch(removePost(_id));
  };

  return (
    <div>
      <Button
        variant="danger"
        className="btn btn-danger mx-3"
        onClick={handleShow}
      >
        delete{" "}
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>are sur to delete {title} post </Modal.Title>
        </Modal.Header>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="danger" onClick={deltePost}>
            delete
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default DeletePost;
