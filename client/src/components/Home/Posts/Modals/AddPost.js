import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import { useDispatch } from "react-redux";
import {
  addNewPost,
  updatePost,
} from "../../../../redux/action/Posts/postsAction";
const AddPost = (props) => {
  const { id } = props;
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const dispatch = useDispatch();
  const [saveInput, setSaveInput] = useState([]);
  const [imageUpload, setImageUpload] = useState([]);

  const saveInputData = (e) => {
    setSaveInput({ ...saveInput, [e.target.name]: e.target.value });
  };

  const sendData = (e) => {
    e.preventDefault();
    dispatch(addNewPost({ ...saveInput, imageUpload }));
    handleClose();
  };
  return (
    <div>
      <div className="container">
        <div className="row">
          <div className="col-8">
            <Button variant="primary btn-add-post" onClick={handleShow}>
              +
            </Button>

            <Modal show={show} onHide={handleClose}>
              <Modal.Header closeButton>
                <Modal.Title>update post</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <Form>
                  <Form.Group
                    className="mb-3"
                    controlId="exampleForm.ControlInput1"
                  >
                    <Form.Label>title</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder=" set new title"
                      autoFocus
                      name="title"
                      onChange={saveInputData}
                    />
                  </Form.Group>

                  <Form.Group
                    className="mb-3"
                    controlId="exampleForm.ControlTextarea1"
                  >
                    <Form.Label>description</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows={3}
                      name="description"
                      onChange={saveInputData}
                    />
                  </Form.Group>

                  <Form.Group
                    className="mb-3"
                    controlId="exampleForm.ControlInput1"
                  >
                    <Form.Label>category</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder=" set new title"
                      name="category"
                      onChange={saveInputData}
                    />
                  </Form.Group>

                  <Form.Group
                    className="mb-3"
                    controlId="exampleForm.ControlInput1"
                  >
                    <Form.Label>image</Form.Label>
                    <Form.Control
                      type="file"
                      name="image"
                      onChange={(e) => setImageUpload(e.target.files[0])}
                    />
                  </Form.Group>
                </Form>
              </Modal.Body>
              <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                  Close
                </Button>
                <Button variant="primary" onClick={(e) => sendData(e)}>
                  Save Changes
                </Button>
              </Modal.Footer>
            </Modal>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddPost;
