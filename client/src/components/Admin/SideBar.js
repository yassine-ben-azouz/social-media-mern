import React from "react";
import { Link } from "react-router-dom";
import "./Admin.css";
const SideBar = () => {
  return (
    <div className="col-3">
      <div className="sider-bar-nav">
        <Link to="/admin" className="sider-bar-nav-brand">
          dashboread
        </Link>
        <ul className="sider-bar-links">
          <li className="sider-bar-link">
            <i className="fa-regular fa-user side-bar-icon"></i>
            <Link to="/admin/users" className="side-bar-title">
              users
            </Link>
          </li>

          <li className="sider-bar-link">
            <i className="fa-regular fa-envelope side-bar-icon"></i>
            <Link to="/admin/register" className="side-bar-title">
              register
            </Link>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default SideBar;
