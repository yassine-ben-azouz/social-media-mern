import React, { useEffect, useState } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { addNewPost, getAllPosts } from "../../redux/action/Posts/postsAction";
import { getAllUsers } from "../../redux/action/Users/userActions";

import "./Admin.css";

const MainContent = () => {
  // declartion state for save data inputs login
  const [newPostData, setNewPostData] = useState({});
  const [imageUpload, setImageUpload] = useState({});
  const getDataInputs = (e) => {
    setNewPostData({ ...newPostData, [e.target.name]: e.target.value });
  };
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { usersList } = useSelector((state) => state.users);
  const { postList, PostsCount } = useSelector((state) => state.posts);

  const postListLength = postList.length;
  const ordersList = [];

  const data = "";
  useEffect(() => {
    dispatch(getAllUsers());
    dispatch(getAllPosts());
  }, []);

  const sendNewProduct = (e) => {
    e.preventDefault();
    dispatch(addNewPost({ ...newPostData, imageUpload }));
  };
  return (
    <div className="col-8">
      <div className="main-content">
        {" "}
        <div className="header-nav">
          <div className="header-links">
            <div className="header-link">
              <h3>users</h3>
              <h3>count {usersList.length || "0"}</h3>
              <div className="details">
                <h3 onClick={() => navigate("/admin/users/")}>see all</h3>
                <i className="fa-regular fa-user header-icon"></i>
              </div>
            </div>
            <div className="header-link">
              <h3>posts</h3>
              <h3>count {PostsCount || "0"}</h3>
              <div className="details">
                <h3 onClick={() => navigate("/admin/products/")}>see all</h3>
                <i
                  className="fa fa-list-alt header-icon"
                  aria-hidden="true"
                ></i>
              </div>
            </div>
          </div>
        </div>
        <hr />
        <div className="add-new-product">
          <div className="form-container">
            <Form>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label className="text-label">title</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter title"
                  className="form-input"
                  name="title"
                  onChange={(e) => getDataInputs(e)}
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label className="text-label">description</Form.Label>
                <Form.Control
                  className="form-input"
                  type="text"
                  placeholder="description"
                  name="description"
                  onChange={(e) => getDataInputs(e)}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label className="text-label">category</Form.Label>
                <Form.Control
                  className="form-input"
                  type="text"
                  placeholder="write category"
                  name="category"
                  onChange={(e) => getDataInputs(e)}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label className="text-label">photo</Form.Label>
                <Form.Control
                  className="form-input"
                  type="file"
                  placeholder="set image profil"
                  name="image"
                  onChange={(e) => setImageUpload(e.target.files[0])}
                />
              </Form.Group>
              <Button
                variant=""
                type="submit"
                className="btn btn-secondary mx-auto"
                onClick={(e) => sendNewProduct(e)}
              >
                add new post
              </Button>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MainContent;
