import React, { useEffect, useState } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { RegisterUser } from "../../redux/action/Users/userActions";
const Register = () => {
  const navigate = useNavigate();
  const [input, setInput] = useState([]);
  const dispatch = useDispatch();
  const saveInputData = (e) => {
    setInput({ ...input, [e.target.name]: e.target.value });
  };
  const sendData = (e) => {
    e.preventDefault();
    console.log(input);
    dispatch(RegisterUser(input));
  };

  const { isAuth, userId } = useSelector((state) => state.users);
  useEffect(() => {
    isAuth && navigate(`/profil/${userId}`);
  }, [isAuth]);
  return (
    <div className="contianer">
      <div className="row">
        <div className="form-container">
          <div className="col-4">
            <Form className="form">
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label className="from-label">username</Form.Label>
                <Form.Control
                  className="form-input"
                  type="text"
                  placeholder="Enter email"
                  name="username"
                  onChange={saveInputData}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label className="from-label">email address</Form.Label>
                <Form.Control
                  className="form-input"
                  type="email"
                  placeholder="Enter email"
                  name="email"
                  onChange={saveInputData}
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label className="from-label">password</Form.Label>
                <Form.Control
                  className="form-input"
                  type="password"
                  placeholder="Password"
                  name="password"
                  onChange={saveInputData}
                />
              </Form.Group>
              <Button
                variant="primary"
                className="btn-form register-btn"
                type="submit"
                onClick={(e) => sendData(e)}
              >
                sign up
              </Button>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Register;
