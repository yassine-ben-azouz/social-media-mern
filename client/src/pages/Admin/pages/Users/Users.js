import Table from "react-bootstrap/Table";
import React, { Fragment, useEffect } from "react";
import "./users.css";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
// import DeleteUser from "../../../../components/Modals/DeleteUser";
import { getAllUsers } from "../../../../redux/action/Users/userActions";
import SideBar from "../../../../components/Admin/SideBar";
import Loader from "../../../../components/Loader/Loader";
import DeleteUser from "../../../../components/Admin/Modals/DeleteUser";

const Users = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getAllUsers());
  }, []);

  const { usersList, loading } = useSelector((state) => state.users);
  console.log(usersList);
  return (
    <div className="row">
      <SideBar />
      <div className="col-8">
        <Table striped bordered hover>
          <thead>
            <tr>
              <th className="table-header">count</th>
              <th className="table-header">username</th>
              <th className="table-header">email</th>
              <th className="table-header">action</th>
            </tr>
          </thead>
          {usersList.length ? (
            usersList.map((user, index) => (
              <Fragment key={user._id}>
                <tbody>
                  <tr>
                    <td className="talbe-header-count">{index + 1}</td>
                    <td className="table-content-username">
                      {user.username}
                      <img
                        src={user.image.url}
                        alt={user.username}
                        className="user-photo"
                      />
                    </td>
                    <td className="table-content-email">{user.email}</td>
                    <td className="table-action">
                      <Link
                        to={`/admin/profil/${user._id}`}
                        className="btn-details"
                      >
                        view profil
                      </Link>

                      <DeleteUser id={user._id} />
                    </td>
                  </tr>
                </tbody>
              </Fragment>
            ))
          ) : (
            <tbody>
              <tr>
                <td>no users</td>
              </tr>
            </tbody>
          )}
        </Table>
      </div>
      {loading && <Loader />}
    </div>
  );
};

export default Users;
