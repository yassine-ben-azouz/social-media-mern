import React from "react";
import SiderBar from "../../components/Admin/SideBar";
import MainContent from "../../components/Admin/MainContent";

const Admin = () => {
  return (
    <div>
      <div className="row">
        <SiderBar />
        <MainContent />
      </div>
    </div>
  );
};

export default Admin;
