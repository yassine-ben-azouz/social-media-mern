import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom";
import SideBar from "../../components/Admin/SideBar";
import Loader from "../../components/Loader/Loader";
import PositList from "../../components/Home/Posts/PositList";
import {
  getUserInfo,
  UploadPhotoProfile,
} from "../../redux/action/Users/userActions";
// import SideBar from "../../components/Admin/SideBar";
// import GoBackButton from "../../components/Go-Back/GoBackButton";
import "./Profil.css";
const Profil = () => {
  const dispatch = useDispatch();
  const [imageInput, setImageInput] = useState("");
  const params = useParams();
  const { userId, isAdmin, userInfo, loading } = useSelector(
    (state) => state.users
  );
  const id = params.id;
  const postList = userInfo?.posts || [];
  // isAuth
  useEffect(() => {
    dispatch(getUserInfo(id));
  }, [userId]);
  console.log(userInfo);
  // const lengthObj = Object.keys(obj).length;
  const sendUploadPhoto = (e) => {
    e.preventDefault();
    dispatch(UploadPhotoProfile({ imageInput, userId }));
  };

  console.log("Boolean", Boolean(userInfo?.posts));
  return (
    <div className="row">
      {loading && <Loader />}
      {isAdmin && <SideBar />}
      <div className={isAdmin ? "container-profil-admin" : "container-profil"}>
        <div className="wrapper">
          <div className="left">
            <img src={userInfo?.image.url} alt="user" width="100" />
            <form className="form-upload-image-profile">
              {/* // id and htmlFor same name */}
              <label
                htmlFor="image"
                className="fa-solid fa-camera upload-image-label"
              ></label>
              <input
                type="file"
                name="file"
                id="image" // id and htmlFor same name
                className="input-profile-photo"
                onChange={(e) => setImageInput(e.target.files[0])}
              />
              <button className="btn-profile-photo" onClick={sendUploadPhoto}>
                upload
              </button>
            </form>
            <h4>{userInfo?.username}</h4>
            <p>UI Developer</p>
          </div>
          <div className="right">
            <div className="info">
              <h3>Information</h3>
              <div className="info_data">
                <div className="data">
                  <h4>Email</h4>
                  <p>{userInfo?.email}</p>
                </div>
                <div className="data">
                  <h4>Phone</h4>
                  <p>0001-213-998761</p>
                </div>
              </div>
            </div>

            <div className="projects">
              <h3>Projects</h3>
              <div className="projects_data">
                <div className="data">
                  <h4>Recent</h4>
                  <p>Lorem ipsum dolor sit amet.</p>
                </div>
                <div className="data">
                  <h4>Most Viewed</h4>
                  <p>dolor sit amet.</p>
                </div>
              </div>
            </div>

            <div className="social_media">
              <ul>
                <li>
                  <Link to="#">
                    <i className="fab fa-facebook-f"></i>
                  </Link>
                </li>
                <li>
                  <Link to="#">
                    <i className="fab fa-twitter"></i>
                  </Link>
                </li>
                <li>
                  <Link to="#">
                    <i className="fab fa-instagram"></i>
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      {userInfo?.posts && (
        <PositList postList={userInfo?.posts} profil="profil" />
      )}
      {/* <GoBackButton /> */}
    </div>
  );
};

export default Profil;
