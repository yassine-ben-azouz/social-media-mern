import React, { useEffect, useState } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { loginUser } from "../../redux/action/Users/userActions";
const Login = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  // insialisation of state to save inputs data
  const [input, setInput] = useState([]);
  // save data inputs on state
  const saveInputData = (e) => {
    setInput({ ...input, [e.target.name]: e.target.value });
  };
  // send data inputs to reducer
  const sendData = (e) => {
    e.preventDefault();
    console.log(input);
    dispatch(loginUser(input));
  };
  // get
  const { isAuth, userId } = useSelector((state) => state.users);
  useEffect(() => {
    isAuth && navigate(`/profil/${userId}`);
  }, [isAuth]);
  return (
    <div className="contianer">
      <div className="row">
        <div className="form-container">
          <div className="col-4">
            <Form className="form">
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label className="from-label">email address</Form.Label>
                <Form.Control
                  className="form-input"
                  type="email"
                  placeholder="Enter email"
                  name="email"
                  onChange={saveInputData}
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label className="from-label">password</Form.Label>
                <Form.Control
                  className="form-input"
                  type="password"
                  placeholder="Password"
                  name="password"
                  onChange={saveInputData}
                />
              </Form.Group>
              <Button
                variant="primary"
                className="btn-form register-btn"
                type="submit"
                onClick={sendData}
              >
                sign in
              </Button>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
