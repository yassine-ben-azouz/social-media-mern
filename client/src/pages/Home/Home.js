import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import PositList from "../../components/Home/Posts/PositList";
import Loader from "../../components/Loader/Loader";
import { getAllPosts } from "../../redux/action/Posts/postsAction";

const Home = () => {
  const { postList, loading, pagesCount } = useSelector((state) => state.posts);

  const [currentPage, setCurrentPage] = useState(1);
  const dispatch = useDispatch();

  /*======= infinite scroll ========*/
  const onScroll = () => {
    console.log("pagesCount", pagesCount);
    const scrollTop = document.documentElement.scrollTop;
    const clientHeight = document.documentElement.clientHeight;
    const scrollHeight = document.documentElement.scrollHeight - 2;
    if (currentPage < pagesCount) {
      if (scrollTop + clientHeight >= scrollHeight) {
        setTimeout(() => {
          setCurrentPage((prevState) => prevState + 1);
        }, 1000);
      }
    }
  };
  /*=======//  infinite scroll //========*/

  useEffect(() => {
    dispatch(getAllPosts(currentPage));

    // infinite scroll
    window.addEventListener("scroll", onScroll);
    return () => window.removeEventListener("scroll", onScroll);
  }, [currentPage]);
  return (
    <div>
      {loading && <Loader />}
      <PositList postList={postList} />
    </div>
  );
};

export default Home;
