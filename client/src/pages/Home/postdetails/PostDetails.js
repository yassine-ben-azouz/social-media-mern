import React, { useEffect } from "react";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import Comments from "../../../components/Home/Posts/Comments/Comments";
import { toggleComments } from "../../../redux/action/comments/CommentsActions";
import { getPost, ToggleLike } from "../../../redux/action/Posts/postsAction";
import "./PostDetails.css";

const PostDetails = (props) => {
  const { post } = props;
  const params = useParams();
  const id = params.id;
  const { postInfo, toggleComment } = useSelector((state) => state.posts);
  const { isAuth, isAdmin, userId } = useSelector((state) => state.users);
  const checkLike = postInfo?.likes.indexOf(userId?.toString()) + 1;
  const countLikes = postInfo?.likes.length || "";
  const commentsCount = postInfo?.comments.length;
  const dispatch = useDispatch();
  const toggle = () => {
    dispatch(ToggleLike(postInfo._id));
  };
  useEffect(() => {
    dispatch(getPost(id));
  }, []);

  return (
    <div>
      <div className="col-6 my-4 mx-auto ">
        <Card className="text-center post-card">
          <div className="container-post">
            <Card.Header className="header-post">
              <div className="user-info">
                <h1 className="header-username-post">
                  {postInfo?.owner.username}
                </h1>
                <img
                  src={postInfo?.owner.image.url}
                  alt={postInfo?.owner.username}
                  className="post-header-image"
                />
              </div>
            </Card.Header>

            <Card.Body className="post-body">
              <Card.Title>{postInfo?.title}</Card.Title>

              <img
                src={postInfo?.image.url}
                alt={postInfo?.title}
                className="image-post"
              />
              <Card.Text className="post-description">
                {postInfo?.description}
              </Card.Text>
            </Card.Body>
            {/* <Card.Footer className="text-muted">2 days ago</Card.Footer> */}

            <div className="post-action">
              <div className="post-action-likes">
                <i
                  className={
                    checkLike
                      ? "fa fa-thumbs-up like-icon"
                      : "fa fa-thumbs-up  unlike-icon"
                  }
                  aria-hidden="true"
                  onClick={toggle}
                ></i>
                <p className="count-likes">{countLikes}</p>
              </div>

              <p
                className="post-action-comment"
                onClick={() => dispatch(toggleComments())}
              >
                comments {commentsCount}
              </p>
            </div>
            <div className="">
              {toggleComment && <Comments postInfo={postInfo} />}
            </div>
          </div>
        </Card>
      </div>
    </div>
  );
};

export default PostDetails;
